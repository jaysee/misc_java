package playing.with.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class PlayingWithProperties {

    static Properties prop;
    
    public static void main(String[] args) {
        makeProperties();
    }
    
    public static void makeProperties()
    {
        try
        {
            prop=new Properties();
            prop.load(new FileInputStream("test.properties"));
            prop.setProperty("test1", "This is a test");
            prop.setProperty("test2", "This is another test");
            prop.list(System.out);
            prop.store(new FileOutputStream("test.properties"), null);
        } catch (Exception e) {
            
            e.printStackTrace();
            
        }
    }
}
